# Video club ORM

Proyecto base orientado al lado del servidor en el cual se utiliza sequelize como ORM para manejar una base de datos hecha en MySQL la cual esta montada en un contenedor de Docker

## Diagrama de clases
![Image text](./public/images/diagrama%20er.png)

Los modelos estan definidos en la carpeta models y la conexión con la base de datos y la relación entre modelos esta definida en el archivo db.js
