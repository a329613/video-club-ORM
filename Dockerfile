FROM node
LABEL Jose Carlos Chaparro Morales
WORKDIR /app
COPY . .
ENV HOME video-club
RUN npm install
EXPOSE 3000
CMD npm start