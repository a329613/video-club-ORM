const Sequelize = require('sequelize');

const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
const directorModel = require('./models/director');
const memberModel = require('./models/member');
const copyModel = require('./models/copy');
const bookingModel = require('./models/booking');
const actorModel = require('./models/actor');
const movieActorModel = require('./models/movieActor');
// 1) Nombre de la base de datos
// 2) Usuario de la base de datos
// 3) Contraseña de la base de datos
// 4) Objeto de configuración (ORM)

const sequelize = new Sequelize('video_club','root', 'secret',{
    host: 'localhost',
    dialect: 'mysql'
});

const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Director = directorModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize,Sequelize);


//Un genero tiene muchas peliculas
Genre.hasMany(Movie, {as: 'movies'});
//Una pelicula tiene un genero
Movie.belongsTo(Genre, {as: 'genre'});
//Un director tiene muchas peliculas
Director.hasMany(Movie, {as: 'movies'});
//Una pelicula tiene un director
Movie.belongsTo(Director, {as: 'director'});


//Un movie tiene muchas copias
Movie.hasMany(Copy, {as: 'copies'});
//Una copia tiene una movie
Copy.belongsTo(Movie, {as: 'movie'});


//Una copia tiene muchas reservaciones
Copy.hasMany(Booking, {as: 'bookings'});
//Una reservación tiene tiene una peli
Booking.belongsTo(Copy, {as: 'copy'});
//Un miembro tiene muchas reservaciones
Member.hasMany(Booking, {as: 'bookings'});
//Una reservacion tiene un miembro
Booking.belongsTo(Member, {as: 'member'});

//Un actor participa en muchas peliculas
MovieActor.belongsTo(Movie, {foreignKey: 'movieId'});

//En una pelicula participan muchos actores
MovieActor.belongsTo(Actor, {foreignKey:'actorId'});

Movie.belongsToMany(Actor,{
    foreignKey:'actorId',
    as:'actors',
    through:'moviesActors'
});

Actor.belongsToMany(Movie,{
    foreignKey:'movieId',
    as:'movies',
    through:'moviesActors'
})

//force no se usa en produccion
sequelize.sync({
    force:true,
}).then(()=>{
    console.log("Base de datos actualizada");
});

module.exports = { Genre, Movie, Director, Member, Booking, Copy, Actor };
