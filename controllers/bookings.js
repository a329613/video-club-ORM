const express = require('express');
const { Booking } = require('../db');

//method url action

//GET /users/  list
function list(req, res, next) {
    Booking.findAll({include:['member','copy']})
                    .then(objects => res.json(objects))
                    .catch(err => res.send(err));
  };

//GET /users/{id}  index
function index(req, res, next) {

  const id = req.params.id;
  Booking.findByPk(id).then(object => res.json(object))
                    .catch(err => res.send(err));
};

//POST /users/  create
function create(req, res, next) {
  
  const date = req.body.date;
  const memberId = req.body.memberId;
  const copyId = req.body.copyId;

  let booking = new Object({
    date:date,
    memberId:memberId,
    copyId:copyId
  });
  console.log(`Hola estas creando una renta en la base de datos con date= ${date} y memberId = ${memberId}`);

  Booking.create(booking)
       .then(obj => res.json(obj))
       .catch(err => res.send(err));

  
  console.log(`Hola estas creando una renta en la base de datos con date= ${date} y memberId = ${memberId}`);

  }

    //res.send(`create => parametros ${name}, ${lastName}`);


//PUT /users/{id}  replace
function replace(req, res, next) {
  //res.send('respond with replace');
  const id = req.params.id;

  Booking.findByPk(id).then( (object) => {
    const date = req.body.date ? req.body.date : null;
    const memberId = req.body.memberId  ? req.body.memberId : null;
    const copyId = req.body.copyId  ? req.body.copyId : null;

    object.update({date:date, memberId:memberId, copyId:copyId})
          .then(Booking => res.json(Booking))
          .catch(err => res.send(err));
  }).catch(err => res.send(err));
  
  

};

//PATCH /users/{id} update
function update(req, res, next) {
  //res.send('respond with update');
  const id = req.params.id;

  Booking.findByPk(id).then( (object) => {
    const date = req.body.date ? req.body.date : object.date;
    const memberId = req.body.memberId  ? req.body.memberId : object.memberId;
    const copyId = req.body.copyId  ? req.body.copyId : object.copyId;

    object.update({date:date, memberId:memberId, copyId:copyId})
          .then(Booking => res.json(Booking))
          .catch(err => res.send(err));

  }).catch(err => res.send(err));
  
};


//DELETE /users/{id} destroy
function destroy(req, res, next) {
  const id = req.params.id;
  Booking.destroy({where:{id:id}})
        .then(obj => res.json(obj))
        .catch(err => res.send(err))
  //res.send('respond with destroy');
};


module.exports = { list, index, create, replace, update, destroy };