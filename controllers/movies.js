const express = require('express');
const { Movie, Actor } = require('../db');

//method url action

//GET /users/  list
function list(req, res, next) {
    Movie.findAll({include:['genre','director','actors']})
                    .then(objects => res.json(objects))
                    .catch(err => res.send(err));
    //res.send('respond with list');
  };

//GET /users/{id}  index
function index(req, res, next) {
  const id = req.params.id;
  Movie.findByPk(id).then(object => res.json(object))
                    .catch(err => res.send(err));

};

//POST /users/  create
function create(req, res, next) {
  
  const title = req.body.title;
  const genreId = req.body.genreId;
  const directorId = req.body.directorId;

  let movie = new Object({
    title:title,
    genreId:genreId,
    directorId:directorId
  });
  console.log(`Hola estas creando un pibe en la base de datos con title= ${title} y genreId = ${genreId}`);

  Movie.create(movie)
       .then(obj => res.json(obj))
       .catch(err => res.send(err));

  
  console.log(`Hola estas creando un pibe en la base de datos con title= ${title} y genreId = ${genreId}`);

  };


function addActor(req, res, next){
  const idMovie = req.body.idMovie;
  const idActor = req.body.idActor;
  console.log('HOla')
  Movie.findByPk(idMovie).then((movie) =>{
    Actor.findByPk(idActor).then(actor =>{
      movie.addActor(actor);
      res.json(movie);
    }).catch(err => res.send(err))
  } ).catch(err => res.send(err));
}

    //res.send(`create => parametros ${name}, ${lastName}`);


//PUT /users/{id}  replace
function replace(req, res, next) {
  //res.send('respond with replace');
  const id = req.params.id;

  Movie.findByPk(id).then( (object) => {
    const title = req.body.title ? req.body.title : "";
    const status = req.body.status  ? req.body.status : false;

    object.update({title:title, status:status})
          .then(Movie => res.json(Movie))
          .catch(err => res.send(err));

  }).catch(err => res.send(err));
  
  

};

//PATCH /users/{id} update
function update(req, res, next) {
  const id = req.params.id;

  Movie.findByPk(id).then( (object) => {
    const title = req.body.title ? req.body.title : object.title;
    const status = req.body.status  ? req.body.status : object.status;

    object.update({title:title, status:status})
          .then(Movie => res.json(Movie))
          .catch(err => res.send(err));

  }).catch(err => res.send(err));
  
};


//DELETE /users/{id} destroy
function destroy(req, res, next) {
  const id = req.params.id;
  Movie.destroy({where:{id:id}})
        .then(obj => res.json(obj))
        .catch(err => res.send(err))
};


module.exports = { list, index, create, replace, update, destroy, addActor};