const express = require('express');
const { Actor } = require('../db');

//method url action

//GET /users/  list
function list(req, res, next) {
    Actor.findAll({include:['movies']})
    .then(objects => res.json(objects))
    .catch(err => res.send(err));
  };

//GET /users/{id}  index
function index(req, res, next) {
  const id = req.params.id;
  Actor.findByPk(id)
  .then(object => res.json(object))
  .catch(err => res.send(err));
};

//POST /users/  create
function create(req, res, next) {
  
  const name = req.body.name;
  const last_name = req.body.last_name;

  let actor = new Object({
    name:name,
    last_name:last_name
  });

  Actor.create(actor)
       .then(obj => res.json(obj))
       .catch(err => res.send(err));

  };

    //res.send(`create => parametros ${name}, ${lastName}`);


//PUT /users/{id}  replace
function replace(req, res, next) {
  //res.send('respond with replace');
  const id = req.params.id;

  Actor.findByPk(id).then( (object) => {
    const name = req.body.name ? req.body.name : "";
    const last_name = req.body.last_name  ? req.body.last_name : "";

    object.update({name:name, last_name:last_name})
          .then(Actor => res.json(Actor))
          .catch(err => res.send(err));

  }).catch(err => res.send(err));
  
  

};

//PATCH /users/{id} update
function update(req, res, next) {
  //res.send('respond with update');
  const id = req.params.id;

  Actor.findByPk(id).then( (object) => {
    const name = req.body.name ? req.body.name : object.name;
    const last_name = req.body.last_name  ? req.body.last_name : object.last_name;

    object.update({name:name, last_name:last_name})
          .then(Actor => res.json(Actor))
          .catch(err => res.send(err));

  }).catch(err => res.send(err));
  
};


//DELETE /users/{id} destroy
function destroy(req, res, next) {
  const id = req.params.id;
  Actor.destroy({where:{id:id}})
        .then(obj => res.json(obj))
        .catch(err => res.send(err))
  //res.send('respond with destroy');
};


module.exports = { list, index, create, replace, update, destroy };