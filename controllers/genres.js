const express = require('express');
const { Genre } = require('../db');

//method url action

//GET /users/  list
function list(req, res, next) {
    Genre.findAll({include:['movies']})
                .then(objects => res.json(objects))
                .catch(err => res.send(err));
    //res.send('respond with list');
  };

//GET /users/{id}  index
function index(req, res, next) {
  //req.params nos permite acceder a los parametros de la url
  //req.boy nos permite acceder a los parametros de la peticion
  const id = req.params.id;
  Genre.findByPk(id).then(object => res.json(object))
                    .catch(err => res.send(err));

  //res.send(`index Parametros => ${id}`);

};

//POST /users/  create
function create(req, res, next) {
  
  const description = req.body.description;
  const status = req.body.status;

  let genre = new Object({
    description:description,
    status:status
  });

  Genre.create(genre)
       .then(obj => res.json(obj))
       .catch(err => res.send(err));

  
  console.log(`Hola estas creando un pibe en la base de datos con description= ${description} y status = ${status}`);

  }

    //res.send(`create => parametros ${name}, ${lastName}`);


//PUT /users/{id}  replace
function replace(req, res, next) {
  //res.send('respond with replace');
  const id = req.params.id;

  Genre.findByPk(id).then( (object) => {
    const description = req.body.description ? req.body.description : "";
    const status = req.body.status  ? req.body.status : false;

    object.update({description:description, status:status})
          .then(genre => res.json(genre))
          .catch(err => res.send(err));

  }).catch(err => res.send(err));

};

//PATCH /users/{id} update
function update(req, res, next) {
  //res.send('respond with update');
  const id = req.params.id;

  Genre.findByPk(id).then( (object) => {
    const description = req.body.description ? req.body.description : object.description;
    const status = req.body.status  ? req.body.status : object.status;

    object.update({description:description, status:status})
          .then(genre => res.json(genre))
          .catch(err => res.send(err));

  }).catch(err => res.send(err));
  
};


//DELETE /users/{id} destroy
function destroy(req, res, next) {
  const id = req.params.id;
  Genre.destroy({where:{id:id}})
        .then(obj => res.json(obj))
        .catch(err => res.send(err))

};


module.exports = { list, index, create, replace, update, destroy };